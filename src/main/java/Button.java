import javax.accessibility.Accessible;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.plaf.metal.MetalButtonUI;
import java.awt.*;
import java.awt.event.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Класс Button наследуется от JButton.
 * Является основным элементом на игровом поле Window.
 * При клике на кнопку она остается в нажатом состоянии.
 * @see JButton
 * @see Window
 */
public class Button extends JButton implements Accessible {

    /** Уникальноый ID кнопки */
    private static final AtomicInteger id = new AtomicInteger(0);
    private final int buttonId;
    /** ID кнопки на которую кликнули */
    private static Integer buttonClickedId;
    /** Координаты кнопки */
    private int rowCoord;
    private int columnCoord;

    /** Конструктор кнопки */
    public Button(int rowCoord, int columnCoord, String symbolInit) {
        // Устанавливаем id кнопки
        buttonId = id.incrementAndGet();
        // Устанавливаем координаты кнопки
        this.setRowCoord(rowCoord);
        this.setColumnCoord(columnCoord);
        // Устанавливаем текст кнопки
        this.setText(symbolInit);
        // Убираем фокус у кнопки по умолчанию
        this.setFocusPainted(false);
        // Добавляем обработчик событий мыши
        this.addMouseListener(new myMouseListener());
    }

    /** @return возвращает id кнопки */
    public int getButtonId() {
        return buttonId;
    }

    /** @return возвращает id кнопки, которая только что была нажата */
    public static Integer getButtonClickedId() {
        if (null == buttonClickedId){
            return -1;
        }
        return buttonClickedId;
    }

    public void setRowCoord(int rowCoord) {
        this.rowCoord = rowCoord;
    }

    public void setColumnCoord(int columnCoord) {
        this.columnCoord = columnCoord;
    }

    /** @return возвращает id кнопки, получая на вход координаты кнопки и размер стороны поля */
    static public int coordToId(int fieldSideSize, int rowCoord, int columnCoord){
        int id = (fieldSideSize * rowCoord) + columnCoord + 1;
        return id;
    }

    /** @return возвращает координату строки, получая на вход id кнопки и размер стороны поля */
    static public int idToRowCoord(int fieldSideSize, int buttonId){
        int rowCoord = (buttonId - 1) / fieldSideSize;
        return rowCoord;
    }

    /** @return возвращает координату столбца, получая на вход id кнопки и размер стороны поля */
    static public int idToColumnCoord(int fieldSize, int buttonId){
        int columnCoord = (buttonId - 1) % fieldSize;
        return columnCoord;
    }

    /** Реализация класса-обработчика события для мыши */
    private class myMouseListener implements MouseListener {

        /** Переопределяем класс клика на кнопку */
        @Override
        public void mouseClicked(MouseEvent ev) {
            try {
                if (Game.isPlayer1MakeMove()) {
                    // Сохраняем id последней нажатой кнопки
                    buttonClickedId = (((Button) ev.getSource()).getButtonId());
                }
            } catch (NumberFormatException e) {
                System.out.println("Ошибка ввода!");
            }
        }

        @Override
        public void mouseEntered(MouseEvent arg0) { }

        @Override
        public void mouseExited(MouseEvent arg0) { }

        @Override
        public void mousePressed(MouseEvent arg0) { }

        @Override
        public void mouseReleased(MouseEvent arg0) { }
    }

    /** Меняем цвет нажатой-задисэйбленной кнопки */
    public void changeColor(String winSymbol){
        Border line;
        if (winSymbol == Players.getSymbolP1()) {
                line = new LineBorder(Color.decode("#F67070"), 2, true);
            } else {
                line = new LineBorder(Color.decode("#7070F6"), 2, true);
            }
        Border margin = new EmptyBorder(1, 1, 1, 1);
        Border compound = new CompoundBorder(line, margin);
        this.setBorder(compound);
        this.setUI(new MetalButtonUI() {
            protected Color getDisabledTextColor() {
                if (winSymbol == Players.getSymbolP1()) {
                    return Color.decode("#F67070");
                } else{
                    return Color.decode("#7070F6");
                }
            }
        });
    }
}
