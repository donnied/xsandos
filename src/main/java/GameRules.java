import org.jetbrains.annotations.NotNull;
import static java.lang.Math.pow;

/**
 * Класс GameRules описывает конкретные правила игры и
 * следит за их выполнением. Проверяет строки по
 * горизонтали, вертикали, и диагонали на наличие
 * одинаковых символов.
 */
public class GameRules extends Game {

    private String symbolP1 = Players.getSymbolP1();
    private String symbolAi = AI.getSymbolAi();
    // Имя победившего игрока
    private String winnerName;

    // Массивы
    private String[] winList;
    private String[] loseList;
    private int countWinColumn;
    private int countWinRow;
    private int countLoseColumn;
    private int countLoseRow;
    private int countWinCrossLR;
    private int countLoseCrossLR;
    private int countWinCrossRL;
    private int countLoseCrossRL;

    // Возвращает true если кто-то победил
    protected boolean checkGameWin(Window window){
        int fieldSideSize = window.getFieldSideSize();

        Button[][] buttonsWinArray = window.getButtonsArray();

        // Правило победы в игре - выстроить символы в ряд
        initWinRow(window);

        for (int i=0; i<fieldSideSize; i++) {
            countWinRow = 0;
            countWinColumn = 0;
            countLoseRow = 0;
            countLoseColumn = 0;
            countWinCrossLR = 0;
            countLoseCrossLR = 0;
            countWinCrossRL = 0;
            countLoseCrossRL = 0;

            for (int j=0; j<fieldSideSize; j++) {
                boolean checkCrossLR = checkCrossLR(j,  fieldSideSize, buttonsWinArray);
                boolean checkCrossRL = checkCrossRL(j,  fieldSideSize, buttonsWinArray);
                boolean checkRows = checkRows(i, j, fieldSideSize, buttonsWinArray);
                boolean checkColumns = checkColumns(i, j, fieldSideSize, buttonsWinArray);

                if (checkCrossLR || checkCrossRL || checkRows || checkColumns){
                    return true;
                }
            }
        }
        return false;
    }

    public String getWinnerName() {
        return winnerName;
    }

    public void setWinnerName(String winnerName) {
        this.winnerName = winnerName;
    }

    // Возвращает true если ничья (закончились ходы)
    protected boolean checkGameDraw(Window window) {
        if (AI.getPushedButtonsId().size() >= (pow(window.getFieldSideSize(), 2)) && !isGameOver()){
            winnerName = "Ничья";
            return true;
        }
        return false;
    }

    // Печатаем сообщение о том, что игра закончена
    protected void printEndMessage(Window window, String winner){
        if (winner == "Ничья"){
            window.setMessage("Ничья...");
            System.out.println("Ничья...");
        }
        else {
            window.setMessage("Победил " + winner);
            System.out.println("Победил " + winner);
            System.out.println("Игра закончена!");
        }
    }

    // Возвращает массив состоящий из символов длинной с ширину поля
    public String[] createList(@NotNull Window window, String[] list, String symbol){
        int length = window.getFieldSideSize();
        for (int i = 0; i<length; i++) {
            list[i] = symbol;
        }
        return list;
    }

    // Правило победы в игре - выстроить одинаковые символы в ряд
    public void initWinRow(@org.jetbrains.annotations.NotNull Window window){
        winList = new String[window.getFieldSideSize()];
        loseList = new String[window.getFieldSideSize()];
        createList(window, winList, symbolP1);
        createList(window, loseList, symbolAi);
    }

    // Возвращает true, если диагональ (слева на право) заполена одинаковыми символами
    private boolean checkCrossLR(int j, int fieldSideSize, Button[][] buttonsWinArray){
        // Проверяем наличие крестов наискосок с лева на право
        if (getButtonsMap().get(Button.coordToId(fieldSideSize, j, j)) == winList[j]) {
            countWinCrossLR++;
            // Если наискосок все значения совпадают, то игра заканчивается
            if (countWinCrossLR >= fieldSideSize){
                for (j=0; j<fieldSideSize; j++) {
                    buttonsWinArray[j][j].changeColor("X");
                }
                setWinnerName("Игрок1");
                return true;
            }
        }
        // Проверяем наличие нулей наискосок с лева на право
        else if(getButtonsMap().get(Button.coordToId(fieldSideSize, j, j)) == loseList[j]) {
            countLoseCrossLR++;
            // Если наискосок все значения совпадают, то игра заканчивается
            if (countLoseCrossLR >= fieldSideSize){
                for (j=0; j<fieldSideSize; j++) {
                    buttonsWinArray[j][j].changeColor("0");
                }
                setWinnerName("ИИ");
                return true;
            }
        }
        return false;
    }

    // Возвращает true, если диагональ (справа на лево) заполена одинаковыми символами
    private boolean checkCrossRL(int i, int fieldSideSize, Button[][] buttonsWinArray){
        // Проверяем наличие крестов наискосок с право на лево
        if (getButtonsMap().get(Button.coordToId(fieldSideSize, i, (fieldSideSize-1)-i)) == winList[i]) {
            countWinCrossRL++;
            // Если наискосок все значения совпадают, то игра заканчивается
            if (countWinCrossRL >= fieldSideSize){
                for (i=0; i<fieldSideSize; i++) {
                    buttonsWinArray[i][(fieldSideSize-1)-i].changeColor("X");
                }
                setWinnerName("Игрок1");
                return true;
            }
        }
        // Проверяем наличие нулей наискосок с право на лево
        else if (getButtonsMap().get(Button.coordToId(fieldSideSize, i, (fieldSideSize-1)-i)) == loseList[i]) {
            countLoseCrossRL++;
            // Если наискосок все значения совпадают, то игра заканчивается
            if (countLoseCrossRL >= fieldSideSize){
                for (i=0; i<fieldSideSize; i++) {
                    buttonsWinArray[i][(fieldSideSize-1)-i].changeColor("0");
                }
                setWinnerName("ИИ");
                return true;
            }
        }
        return false;
    }
    
    // Возвращает true, если найдена колонка заполенная одинаковыми символами
    private boolean checkColumns(int i, int j, int fieldSideSize, Button[][] buttonsWinArray){
        // Проверяем наличие крестов в колонке
        if (getButtonsMap().get(Button.coordToId(fieldSideSize, j, i)) == winList[i]) {
            countWinColumn++;
            // Если в одной колонке все значения совпадают, то игра заканчивается
            if (countWinColumn >= fieldSideSize){
                for (j=0; j<fieldSideSize; j++) {
                    buttonsWinArray[j][i].changeColor("X");
                }
                setWinnerName("Игрок1");
                return true;
            }
        }
        // Проверяем наличие нулей в колонке
        else if (getButtonsMap().get(Button.coordToId(fieldSideSize, j, i)) == loseList[i]) {
            countLoseColumn++;
            // Если в одной колонке все значения совпадают, то игра заканчивается
            if (countLoseColumn >= fieldSideSize){
                for (j=0; j<fieldSideSize; j++) {
                    buttonsWinArray[j][i].changeColor("0");
                }
                setWinnerName("ИИ");
                return true;
            }
        }
            return false;
    }

    // Возвращает true, если найден ряд заполенный одинаковыми символами
    private boolean checkRows(int i, int j, int fieldSideSize, Button[][] buttonsWinArray){
        // Проверяем наличие крестов в ряду
        if (getButtonsMap().get(Button.coordToId(fieldSideSize, i, j)) == winList[j]) {
            countWinRow++;
            // Если в одном ряду все значения совпадают, то игра заканчивается
            if (countWinRow >= fieldSideSize){
                for (j=0; j<fieldSideSize; j++) {
                    buttonsWinArray[i][j].changeColor("X");
                }
                setWinnerName("Игрок1");
                return true;
            }
        }
        // Проверяем наличие нулей в ряду
        else if (getButtonsMap().get(Button.coordToId(fieldSideSize, i, j)) == loseList[j]) {
            countLoseRow++;
            // Если в одном ряду все значения совпадают, то игра заканчивается
            if (countLoseRow >= fieldSideSize){
                for (j=0; j<fieldSideSize; j++) {
                    buttonsWinArray[i][j].changeColor("0");
                }
                setWinnerName("ИИ");
                return true;
            }
        }
        return false;
    }
}
