import java.util.*;

/**
 * Класс Game описывает логику игры.
 * Например кто и в какой последовательности ходит,
 * когда игра заканчивается и тд.
 */
public class Game {

    // Флаг указывает на то, что игра закончена
    private static boolean gameOver = false;

    // Флаг указывает на то, чей сейчас ход
    private static boolean player1MakeMove = true;

    // Храним координаты кнопок и их символ
    private static HashMap<Integer, String> buttonsMap = new HashMap<>();

    // Логика игры
    public void gameStart(){

        Window window = new Window("Крестики и нолики", 4);
        Players player1 = new Players("X");
        AI fedor = new AI("0", 500);
        GameRules rules = new GameRules();

        // Игра продолжается пока есть не нажатые кнопки
        while (AI.getPushedButtonsId().size() < (Math.pow(window.getFieldSideSize(), 2))) {

            // Передаем ход человеку
            if (Game.isPlayer1MakeMove()) {
                window.setMessage("Ваш ход");
                player1.playerMakeMove(window, Button.getButtonClickedId());
            }
            // Передаем ход ИИ
            else {
                window.setMessage("Ходит ИИ");
                fedor.makeMove(window);
            }
            // Проверяем не победил ли кто уже
            if (rules.checkGameWin(window)){
                setGameOver(true);
                rules.printEndMessage(window, rules.getWinnerName());
                // Если кто-то победил то выходим из игры
                break;
            }
        }
        // Проверяем не победил ли кто уже
        if (rules.checkGameWin(window) && !isGameOver()){
            setGameOver(true);
            rules.printEndMessage(window, rules.getWinnerName());
        }
        // Если все кнопки нажаты, то ничья
        else if (rules.checkGameDraw(window) && !isGameOver()){
            setGameOver(true);
            rules.printEndMessage(window, rules.getWinnerName());
        }
    }

    // Возвращает true, если игра закончена
    public static boolean isGameOver() {
        return gameOver;
    }

    // Переключатель конца игры
    public static void setGameOver(boolean gameOver) {
        Game.gameOver = gameOver;
    }

    // Возвращает true, если сейчас ход игрока
    public static boolean isPlayer1MakeMove() {
        return player1MakeMove;
    }

    // Переключатель хода. Ходит игрок или ИИ
    public static void setPlayer1MakeMove(boolean player1MakeMove) {
        Game.player1MakeMove = player1MakeMove;
    }
    // Возвращает карту с координатами кнопок
    public static HashMap<Integer, String> getButtonsMap() {
        return buttonsMap;
    }
}
