import org.jetbrains.annotations.Contract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;

/**
 * Класс AI описывает Искуственный Интелект, который
 * является опонентом Игроку в крестики и нолики
 */
public class AI {

    /** ИИ ходит ноликами */
    static private String symbolAi = "0";
    /** ИИ делает паузу перед своим ходом */
    private int timeOfThinking;
    /** Генератор случайных чисел */
    private Random rnd = new Random();
    /** Храним список кнопок на которые уже кликнули */
    private static List<Integer> pushedButtonsId = new ArrayList<>();

    /** Конструктор ИИ */
    public AI(String symbolAi, int timeOfThinking){
        setSymbolAi(symbolAi);
        this.timeOfThinking = timeOfThinking;
    }

    /** ИИ делает свой ход.
     * То есть кликает на случайную кнопку */
    public void makeMove(Window window){
        // Количество рядов и колонок
        int rows = window.getRows();
        int columns = window.getColumns();
        // Сортируем список кнопок, которые уже были нажаты
        List<Integer> sortedArray = this.sortExclude(pushedButtonsId);
        // Переводим List в обычный массив
        Integer[] array = new Integer[sortedArray.size()];
        array = sortedArray.toArray(array);
        // Получаем id кнопки, случайно выбрав ее из еще не нажатых
        int rndMoveId = getRandomWithExclusion(getRnd(), 1, rows*columns, array);
        // Переводим id в координаты кнопки
        int rndRowCoord = Button.idToRowCoord(rows, rndMoveId);
        int rndColumnCoord = Button.idToColumnCoord(columns, rndMoveId);
        // Размышляем куда пойти
        thinking();
        // Кликаем на кнопку
        this.doClick(window.getButtonsArray()[rndRowCoord][rndColumnCoord], getSymbolAi());
    }

    /** ИИ размышляет куда пойти */
    public void thinking(){
        try {
            TimeUnit.MILLISECONDS.sleep(timeOfThinking);
        } catch (InterruptedException e){
            System.out.println("Что-то пошло не так пока ИИ думал");
        }
    }

    /** Генерируем случайное число в диапазоне от и до,
     *  но не включая числа из упорядоченного массива exclude */
    public int getRandomWithExclusion(Random rnd, int start, int end, Integer... exclude) {
        // Если все кнопки на поле уже нажаты возвращаем -1
        if (exclude.length >= (end - start + 1)){
            System.out.println("ИИ больше некуда ходить!");
            return -1;
        }
        // Если еще есть куда ходить
        else {
            int random = start + rnd.nextInt(end - start + 1 - exclude.length);
            for (int ex : exclude) {
                if (random < ex) {
                    break;
                }
                random++;
            }
            return random;
        }
    }

    // Имитируем нажатие на кнопку
    public void doClick(Button button, String text){
        // При клике на кнопку устанавливаем на нее текст
        button.setText(text);
        // Теперь на кнопку больше нельзя нажимать
        button.setEnabled(false);
        // Запоминаем координаты кнопки на которую кликнули
        AI.addButtonsIdExclude(button.getButtonId());
        // Запоминаем символ на кнопке
        Game.getButtonsMap().put(button.getButtonId(), text);
        // Передаем ход живому игроку
        Game.setPlayer1MakeMove(true);
    }

    @Contract(pure = true)
    static public String getSymbolAi() {
        return symbolAi;
    }

    public void setSymbolAi(String symbolAi) {
        this.symbolAi = symbolAi;
    }

    // Получаем генератор случайных чисел
    public Random getRnd() {
        return rnd;
    }

    // Добавляем кнопку в список кнопок на которые уже кликнули
    public static void addButtonsIdExclude(int buttonsId) {
        AI.getPushedButtonsId().add(buttonsId);
        //System.out.println(AI.getPushedButtonsId());
    }

    // Получаем список кнопок на которые уже кликнули
    @Contract(pure = true)
    public static List<Integer> getPushedButtonsId() {
        return pushedButtonsId;
    }

    // Сортируем список по возрастанию
    public List<Integer> sortExclude(List<Integer> exclude){
        Collections.sort(exclude);
        return exclude;
    }
}
