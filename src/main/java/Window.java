import javax.swing.*;
import java.awt.*;

/**
 * Класс Window {@code Window} наследуется от JFrame и
 * является игровым полем на котором размещаются кнопки и
 * текст с информацией об игре
 */
public class Window extends JFrame {

    /** Размер стороны квадрата окна */
    static private int fieldSideSize;
    /** Количество строк */
    private int rows = fieldSideSize;
    /** Количество колонок */
    private int columns = fieldSideSize;
    /** Массив кнопок */
    private Button[][] buttonsArray;
    /** Символ которым по умолчанию заполняются клетки поля */
    private String fieldSymbol = " ";
    /** Тип разметки */
    private JPanel grid;
    /** Лэйбл отображающий игровые сообщения */
    private JLabel messageLabel;

    /** Конструктор игрового поля по умолчанию */
    public Window(){
        this("Крестики и нолики", 3);
    }

    /** Конструктор игрового поля с настройками заголовка и размера поля */
    public Window(String title, int fieldSideSize){
        // Если не выставить размер и положение то окно будет мелкое и незаметное
        setBounds(100, 100, fieldSideSize*200/3, fieldSideSize*200/3);
        // Это нужно для того чтобы при закрытии окна закрывалась и программа, иначе она останется висеть в процессах
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Устанавливаем заголовок окна по умолчанию
        this.setTitle(title);
        // Устанавливаем размер поля по умолчанию
        this.setFieldSideSize(fieldSideSize);
        this.setRows(fieldSideSize);
        this.setColumns(fieldSideSize);
        // Устанавливаем разметку
        this.setLayout(new BorderLayout());
        // Создание панели с табличным расположением
        grid = new JPanel(new GridLayout(getRows(), getColumns(),1,1));
        this.add(grid, BorderLayout.CENTER);

        // Добавляем кнопки на наше поле
        setButtonArray(getRows(), getColumns(), getFieldSymbol());

        // Добавляем отображение текста о ходе игры
        messageLabel = new JLabel("Ваш ход", JLabel.CENTER);
        this.add(messageLabel, BorderLayout.NORTH);

        // С этого момента приложение запущено
        this.setVisible(true);
    }

    /** Метод добавляющий массив кнопок на наше поле
     * @return возвращает массив кнопок Button
     * @see Button  */
    public Button[][] setButtonArray(int rows, int columns, String fieldSymbol){
        buttonsArray = new Button[rows][columns];
        for (int i=0; i<rows; i++) {
            for (int j=0; j<columns; j++) {
                //buttonsArray[i][j] = new Button(i, j, (String.valueOf(i) + String.valueOf(j)));
                buttonsArray[i][j] = new Button(i, j, fieldSymbol);
                grid.add(buttonsArray[i][j]);
                // Сохраняем buttonsMap. ID -> символ
                Game.getButtonsMap().put(buttonsArray[i][j].getButtonId(), fieldSymbol);
            }
        }
        return buttonsArray;
    }

    /** @return возвращает массив кнопок */
    public Button[][] getButtonsArray(){
        return buttonsArray;
    }

    /** @return возвращает конкретную кнопку из массива кнопок */
    public Button getButton(int buttonId){
        if (buttonId == -1){
            return null;
        }
        return getButtonsArray()[Button.idToRowCoord(rows, buttonId)][Button.idToColumnCoord(columns, buttonId)];
    }

    /** Устанавливает текст сообщения о событии в игре.
     * Например - "Ваш ход" */
    public void setMessage(String message){
        messageLabel.setText(message);
    }

    /** @return возвращает размер стороны игрового поля */
    public int getFieldSideSize() {
        return fieldSideSize;
    }

    /** Устанавливает размер стороны игрового поля */
    public void setFieldSideSize(int fieldSideSize) {
        this.fieldSideSize = fieldSideSize;
    }

    /** @return возвращает количество рядов на игровом поле */
    public int getRows() {
        return rows;
    }

    /** Устанавливает количесво рядов игрового поля */
    public void setRows(int rows) {
        this.rows = rows;
    }

    /** @return возвращает количество колонок на игровом поле */
    public int getColumns() {
        return columns;
    }

    /** Устанавливает количесво колонок игрового поля */
    public void setColumns(int columns) {
        this.columns = columns;
    }

    /** @return возвращает символ устанавливаемый на кнопки по умолчанию */
    public String getFieldSymbol() {
        return fieldSymbol;
    }
}
