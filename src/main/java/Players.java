/**
 * Класс Players описывает игроков в крестики и нолики.
 */
public class Players {

    // Игрок ходит крестиками
    static private String symbolP1 = "X";

    public Players(String symbolP1){
        setSymbolP1(symbolP1);
    }

    /**
     * @param window
     * @param buttonId
     */
    // Игрок делает ход
    public void playerMakeMove(Window window, Integer buttonId){
        if (-1 == buttonId){
            return;
        }
        Button button = window.getButton(buttonId);
        if((button.isEnabled() && !Game.isGameOver())) {
            // При клике на кнопку устанавливаем на нее текст
            button.setText(symbolP1);
            // Теперь на кнопку больше нельзя нажимать
            button.setEnabled(false);
            // Получаем символ с кнопки
            String buttonSymbol = button.getText();
            // Запоминаем координаты кнопки на которую кликнули
            AI.addButtonsIdExclude(button.getButtonId());
            // Запоминаем символ на кнопке
            Game.getButtonsMap().put(button.getButtonId(), buttonSymbol);
            // Передаем ход ИИ
            Game.setPlayer1MakeMove(false);
        }
    }


    public static String getSymbolP1() {
        return symbolP1;
    }

    public static void setSymbolP1(String symbolP1) {
        Players.symbolP1 = symbolP1;
    }
}
