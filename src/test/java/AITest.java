
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.junit.Assert.*;

public class AITest {

    AI ai;
    // ИИ ходит ноликами
    static private String symbolAi = "0";
    // ИИ делает паузу перед своим ходом
    private int timeOfThinking;
    // Генератор случайных чисел
    private Random rnd = new Random();


    @Before
    public void setUp() throws Exception {
        ai = new AI(symbolAi, timeOfThinking);
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testGetRandomWithExclusion()
    {
        assertEquals(4, ai.getRandomWithExclusion(rnd, 1, 5, 1,2,3,5));
        assertEquals(0, ai.getRandomWithExclusion(rnd, 0, 5, 1,2,3,4,5));
        assertEquals(5, ai.getRandomWithExclusion(rnd, 1, 5, 1,2,3,4));
        assertEquals(-1, ai.getRandomWithExclusion(rnd, 1, 5, 1,2,3,4,5));
    }

    @Test
    public void testSortExclude(){
        List<Integer> list = new ArrayList<>();
        list.add(0); list.add(12); list.add(8); list.add(2);
        assertThat(ai.sortExclude(list), contains(0,2,8,12));
    }

    @Test
    public void testSortExcludeEmpty(){
        assertThat(ai.sortExclude(new ArrayList<>()), IsEmptyCollection.empty());
    }
}